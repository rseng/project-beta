from django.contrib import admin
from .models import AutomobileVO, SalesPerson, SalesRecord, Customer

# Register your models here.

admin.site.register(AutomobileVO)
admin.site.register(SalesPerson)
admin.site.register(SalesRecord)
admin.site.register(Customer)



class AutomobileVOAdmin(admin.ModelAdmin):
    pass

class SalesPersonAdmin(admin.ModelAdmin):
    pass

class CustomerAdmin(admin.ModelAdmin):
    pass

class SalesRecordAdmin(admin.ModelAdmin):
    pass
