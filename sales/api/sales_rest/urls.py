
from django.urls import path, include
from .views import sales_person_list, sales_person_detail, customer_list,customer_detail, automobile_vo_list, automobile_vo_delete, sales_record_list

urlpatterns = [
    path("sales_persons/", sales_person_list, name = "sales_person_list"),
    path("sales_persons/<int:pk>/", sales_person_detail, name="sales_person_detail"),
    path("customers/", customer_list, name="customer_list"),
    path("customers/<int:pk>/", customer_detail, name="customer_detail"),
    path("automobileVOs/",automobile_vo_list, name ="automobile_vo_list"),
    path("automobileVO/<int:pk>/", automobile_vo_delete, name="automobile_vo_delete"),
    path("sales_records/",sales_record_list, name ="sales_record_list"),
]
