from django.db import models

# Create your models here.
class AutomobileVO(models.Model):
    color = models.CharField(max_length=50)
    year = models.PositiveSmallIntegerField()
    import_vin = models.CharField(max_length=17, unique=True, null=True)
    import_href = models.CharField(max_length=100, unique=True, null=True)


    def __str__(self):
        return f"{self.import_vin}"

class SalesPerson(models.Model):
    name = models.CharField(max_length =200)
    employee_number = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return f"{self.name}"

class Customer(models.Model):
    name = models.CharField(max_length =200)
    address = models.CharField(max_length =200)
    phone_number = models.CharField(max_length=10)

    def __str__(self):
        return f"{self.name}"

class SalesRecord(models.Model):
    sales_price = models.CharField(max_length=255)

    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )

    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )

    customer = models.ForeignKey(
        Customer,
        related_name="sales_records",
        on_delete=models.PROTECT,
    )
