from django.shortcuts import render
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
from django.http import JsonResponse
import json


from .models import AutomobileVO, SalesPerson, Customer, SalesRecord
# Create your views here.

class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["color", "year", "import_vin", "id", "import_href"]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number", "id"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["name", "address", "phone_number","id"]


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["sales_price", "automobile", "sales_person", "customer", "id"]

    encoders = {
        "sales_person" : SalesPersonEncoder(),
        "customer" : CustomerEncoder(),
        "automobile" : AutomobileVOEncoder(),
    }









@require_http_methods(["GET", "POST"])
def sales_person_list(request):
    if request.method == "GET":
        list_of_sales_person = SalesPerson.objects.all()
        return JsonResponse(
            {"list_of_sales_person": list_of_sales_person}, encoder=SalesPersonEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.create(**content)
            return JsonResponse(
                sales_person, encoder=SalesPersonEncoder, safe=False
                )
        except:
            response = JsonResponse(
                {"message": "This sales person can not be created"}
            )
            response.status_code = 400
            return response


@require_http_methods(["GET","DELETE"])
def sales_person_detail(request, pk):
    if request.method == "GET":
        try:
            sales_person = SalesPerson.objects.get(id=pk)
            return JsonResponse(sales_person, encoder=SalesPersonEncoder, safe=False)
        except:
            response = JsonResponse(
                {"Message": "This sales person doesn't exist!"}
                )
            return response
    else:
        if request.method =="DELETE":
            count, _=SalesPerson.objects.filter(id=pk).delete()
            return JsonResponse({"Deleted": "You've just been erased - Arnold Schwarzenegger"})



@require_http_methods(["GET","POST"])
def customer_list(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerEncoder)
    else:
        try:
            content= json.loads(request.body)
            customer = Customer.objects.create(**content)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except: response = JsonResponse(
            {"message": "Customer can't be created"}
            )
        return response


@require_http_methods(["GET","DELETE"])
def customer_detail(request,pk):
    if request.method == "GET":
        try:
            customer = Customer.objects.get(id=pk)
            return JsonResponse(customer, encoder=CustomerEncoder, safe=False)
        except:
            response = JsonResponse(
            {"Message": "This customer doesn't exist!"}
            )
            return response
    else:
        if request.method == "DELETE":
            count,_=Customer.objects.filter(id=pk).delete()
            return JsonResponse({"Message": "DELETED! WE BE LOSING MONEY IF WE BE LOSING CUSTOMERS!"})


@require_http_methods(["GET"])
def automobile_vo_list(request):
    if request.method == "GET":
        automobiles = AutomobileVO.objects.all()
        return JsonResponse(
            {"automobiles": automobiles},
            encoder=AutomobileVOEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def automobile_vo_delete(request, pk):
    if request.method == "DELETE":
        count, _ = AutomobileVO.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})



@require_http_methods(["GET","POST"])
def sales_record_list(request):
    if request.method == "GET":
        sales_list = SalesRecord.objects.all()
        return JsonResponse({"sales_list": sales_list}, encoder=SalesRecordEncoder, safe=False)
    else:

        content = json.loads(request.body)
        content = {
            **content,
            "automobile": AutomobileVO.objects.get(import_vin=content["automobile"]),
            "sales_person": SalesPerson.objects.get(pk=content["sales_person"]),
            "customer": Customer.objects.get(pk=content["customer"]),
        }
        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            {"sales_record": sales_record}, encoder=SalesRecordEncoder,)
        # except:
        #     pass
            # return JsonResponse({"Message": "This sales record can not be created.  Check to see if something invalid was entered"},safe=False)



