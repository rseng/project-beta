import django
import os
import sys
import time
import json
import requests

sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "service_project.settings")
django.setup()

# Import models from service_rest, here.
# from service_rest.models import Something
from service_rest.models import AutoVO

def get_autos():
    try:
        response=requests.get("http://inventory-api:8000/api/automobiles/")
        content=json.loads(response.content)
        print("get auto poll starting, content: ", content)
        for auto in content["autos"]:
            auto_vo=AutoVO.objects.update_or_create(
                defaults={
                    "vin":auto["vin"],
                    "year":auto["year"],
                    "color":auto["color"],
                })
        return auto_vo
    except:
        pass


def poll():
    while True:
        print('Service poller polling for data')
        try:
           get_autos()
           print("getting autos")
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(10)


if __name__ == "__main__":
    poll()
