from django.urls import path
from .views import (api_list_appointments, api_show_appointments,
                    api_list_autovos, api_show_autovos, api_show_service,
                    api_list_techs, api_show_techs )

urlpatterns=[
    path("listapps/", api_list_appointments, name="api_list_apps"),
    path("listapps/<int:pk>/", api_show_appointments, name="api_show_apps"),
    path("service/<str:vin>/", api_show_service, name="detail_service"),
    path("techs/",api_list_techs, name="api_list_techs"),
    path("techs/<int:pk>/", api_show_techs, name="api_show_techs"),
    path("listvos/", api_list_autovos, name="api_list_autovos"),
    path("showvos/<int:pk>/", api_show_autovos, name="api_show_autovos")
]
