from django.db import models
from django.urls import reverse

# Create your models here.
class AutoVO(models.Model):
    color=models.CharField(max_length=50)
    year=models.PositiveSmallIntegerField()
    vin=models.CharField(max_length=17, unique=True)
    def __str__(self):
        return self.vin
    def get_api_url(self):
        return reverse("api_show_autovos",kwargs={"pk":self.id})

class AutoTech(models.Model):
    name=models.CharField(max_length=200)
    employee_number=models.PositiveSmallIntegerField(unique=True)
    def __str__(self):
        return self.name
    def get_api_url(self):
        return reverse("api_show_techs", kwargs={"pk":self.id})

class Appointment(models.Model):
    customer_name=models.CharField(max_length=200)
    date=models.DateTimeField(null=True)
    reason=models.CharField(max_length=200)
    is_finished=models.BooleanField(default=False)
    auto_tech=models.ForeignKey(
        AutoTech,
        related_name="appointments",
        on_delete=models.PROTECT,
    )
    is_vip=models.BooleanField(default=False)
    vin=models.ForeignKey(
        AutoVO,
        related_name="appointments",
        on_delete=models.PROTECT,
    )

    def __str__(self):
        return self.customer_name
    def get_api_url(self):
        return reverse("api_show_appointments", kwargs={"pk":self.id})
