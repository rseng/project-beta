from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
from .models import AutoTech, AutoVO, Appointment
# Create your views here.
class AutoTechEncoder(ModelEncoder):
    model=AutoTech
    properties=[
        "employee_number",
        "name",
    ]
class AutoVOListEncoder(ModelEncoder):
    model=AutoVO
    properties=["year","vin"]
class AutoVODetailEncoder(ModelEncoder):
    model=AutoVO
    properties=["color","year","vin"]

class AppointmentListEncoder(ModelEncoder):
    model=Appointment
    properties=[
        "id",
        "customer_name",
        "date",
        "reason",
        "is_finished",
        "is_vip",
    ]
    encoders={
        "vin":AutoVODetailEncoder(),
        "auto_tech":AutoTechEncoder(),
    }
    def get_extra_data(self, o):
        count=AutoVO.objects.filter(vin=o.vin).count()
        auto_tech_name=o.auto_tech.name
        auto_tech_number=o.auto_tech.employee_number
        return{
            "vin":o.vin.vin,
            "is_vip":count>0,
            "auto_tech":{
                "name":auto_tech_name,
                "number":auto_tech_number
            }
        }


class AppointmentDetailEncoder(ModelEncoder):
    model=Appointment
    properties=[
        "id",
        "customer_name",
        "date",
        "reason",
        "is_finished",
    ]
    encoders={
        "vin":AutoVODetailEncoder(),
        "auto_tech":AutoTechEncoder(),
    }
    def get_extra_data(self, o):
        count=AutoVO.objects.filter(vin=o.vin).count()
        auto_tech_name=o.auto_tech.name
        auto_tech_number=o.auto_tech.employee_number
        return{
            "vin":o.vin.vin,
            "is_vip":count>0,
            "auto_tech":{
                "name":auto_tech_name,
                "number":auto_tech_number
            }
        }

#enter a service appointment (post) and get list of appointments (get)
@require_http_methods(["GET","POST"])
def api_list_appointments(request):
    if request.method == "GET":
        appointments =Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointments},
            encoder = AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            number = content["auto_tech"]
            tech = AutoTech.objects.get(employee_number=number)
            content["auto_tech"] = tech
            vin=content["vin"]
            auto_vin=AutoVO.objects.get(vin=vin)
            content["vin"]=auto_vin
        except AutoTech.DoesNotExist:
            return JsonResponse(
            {"alert": "Invalid Tech, Try Again"},
            status=400
            )
        service = Appointment.objects.create(**content)
        return JsonResponse(
            service,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )

#show/list appointments for a specific Vin (pk) #
@require_http_methods(["GET","PUT", "DELETE"])
def api_show_appointments(request, pk):
    if request.method=="GET":
        try:
            appointment=Appointment.objects.get(id=pk)
            return JsonResponse(
                appointment,
                encoder=AppointmentDetailEncoder,
                safe=False
                )
        except Appointment.DoesNotExist:
            response=JsonResponse({"alert":"This Appointment does not exist"})
            response.status_code=404
            return response
    elif request.method=="DELETE":
        try:
            appointment=Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse(
                {"alert":"Appointment is deleted"}
            )
        except Appointment.DoesNotExist:
            response=JsonResponse({"alert":"This Appointment does not exist"})
            response.status_code=404
            return response
    else:
        content=json.loads(request.body)
        vin=content["vin"]
        auto_vin=AutoVO.objects.get(vin=vin)
        content["vin"]=auto_vin
        appointment=Appointment.objects.filter(id=pk).update(**content)
        print(appointment)
        appointment=Appointment.objects.get(id=pk)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )

#show overall service hsitory for particular vin i.e vehicle
@require_http_methods(["GET", "DELETE", "PUT"])
def api_show_service(request, vin):
    if request.method=="GET":
        try:
            autoVOobj = AutoVO.objects.get(vin=vin)
            appointments=Appointment.objects.get(vin=autoVOobj)
            return JsonResponse(
                appointments,
                encoder=AppointmentDetailEncoder,
                safe=False
        )
        except Appointment.DoesNotExist:
            response=JsonResponse({"alert":"This Appointment does not exist"})
            response.status_code=404
            return response
    elif request.method=="DELETE":
        try:
            autoVOobj = AutoVO.objects.get(vin=vin)
            appointments=Appointment.objects.get(vin=autoVOobj)
            appointments.delete()
            return JsonResponse(
                {"alert":"Appointment is deleted"}
            )
        except Appointment.DoesNotExist:
            response=JsonResponse({"alert":"This Appointment does not exist"})
            response.status_code=404
            return response
    else:
        content=json.loads(request.body)
        appointment=Appointment.objects.filter(vin=vin).update(**content)
        appointment=Appointment.objects.get_or_create(vin=vin)
        return JsonResponse(
            appointment,
            encoder=AppointmentDetailEncoder,
            safe=False,
        )

#enter a technician (post) and get a list of technicians (get?) (get, post method)
@require_http_methods(["GET","POST"])
def api_list_techs(request):
    if request.method == "GET":
        techs = AutoTech.objects.all()
        return JsonResponse(
            {"techs": techs},
            encoder=AutoTechEncoder,
        )
    else:
        content=json.loads(request.body)
        auto_tech=AutoTech.objects.create(**content)
        return JsonResponse(
            auto_tech,
            encoder=AutoTechEncoder,
            safe=False,
        )

#list details of techs available at service department
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_techs(request,pk):
    if request.method=="GET":
        try:
            auto_tech=AutoTech.objects.get(id=pk)
            return JsonResponse(
                auto_tech,
                encoder=AutoTechEncoder,
                safe=False
            )
        except AutoTech.DoesNotExist:
            response=JsonResponse({"alert":"This Tech does not exist"})
            response.status_code=404
            return response
    elif request.method=="DELETE":
        try:
            auto_tech=AutoTech.objects.get(id=pk)
            auto_tech.delete()
            return JsonResponse(
                {"alert":"This Tech is now deleted"}
            )
        except AutoTech.DoesNotExist:
            response=JsonResponse({"alert":"This Appointment does not exist"})
            response.status_code=404
            return response
    else:
        content=json.loads(request.body)
        auto_tech=AutoTech.objects.filter(id=pk).update(**content)
        auto_tech=AutoTech.objects.get(id=pk)
        return JsonResponse(
            auto_tech,
            encoder=AutoTechEncoder,
            safe=False,
        )

@require_http_methods(["GET","POST"])
def api_list_autovos(request):
    if request.method=="GET":
        autovos=AutoVO.objects.all()
        return JsonResponse(
           {"auto_vos":autovos},
           encoder=AutoVOListEncoder,
        )
    else:
        try:
            content=json.loads(request.body)
            auto_vos=AutoVO.objects.create(**content)
            return JsonResponse(
                auto_vos,
                encoder=AutoVODetailEncoder,
                safe=False,
            )
        except:
            return JsonResponse(
                {"auto_vo":"can't create"},
                status=400,
            )

@require_http_methods(["GET","PUT","DELETE"])
def api_show_autovos(request,pk):
    if request.method=="GET":
        try:
            autovos=AutoVO.objects.get(id=pk)
            return JsonResponse(
                autovos,
                encoder=AutoVODetailEncoder,
                safe=False
            )
        except AutoVO.DoesNotExist:
            response=JsonResponse({"alert":"This AutoVO does not exist"})
            response.status_code=404
            return response
    elif request.method=="DELETE":
        try:
            autovos=AutoVO.objects.get(id=pk)
            autovos.delete()
            return JsonResponse(
                {"alert":"This VO is now deleted"}
            )
        except AutoVO.DoesNotExist:
            response=JsonResponse({"alert":"This VO does not exist"})
            response.status_code=404
            return response
    else:
        content=json.loads(request.body)
        autovos=AutoVO.objects.filter(id=pk).update(**content)
        autovos=AutoVO.objects.get(id=pk)
        return JsonResponse(
            autovos,
            encoder=AutoVODetailEncoder,
            safe=False,
        )
