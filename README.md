# CarCar

Team:

* Ron Seng - Sales microservice
* Craig Celestin - Service microservice


-----Instructions-----
Set up

1. Open terminal

2. Create a new directory to place project into.
    -mkdir << name of directory >>

3. cd into the directory that has just been created
    -cd << name of directory >>

4. Fork and then Clone the project into that directory
    - git clone https://gitlab.com/rseng/project-beta

5. Open up the project in VSC
    -code .

6. Create a database for the project via volume
    -docker volume create beta-data

7. Build docker containers
    -docker compose build

8. Spin up containers
    -docker compose up

-----End of Instructions-----




## Design
-----Sales Design------

![](Images/Screen%20Shot%202022-10-28%20at%208.45.51%20AM.png)


## Sales microservice

![](Images/Screen%20Shot%202022-10-28%20at%209.22.41%20AM.png)


-----INSOMNIA FOR SALES-----

---KEY NOTES---
-Order of creation for an automobile
1. Manufacturer
2. Vehicle Model
3. Automobile


-A sales record CAN NOT be created until the following are in the database for selection
1. AutomobileVO with pertinent information from Poller
2. Salesperson
3. Customer

______________________________________________________________________________________________________________________________________
///////////AutomobileVO Insomnia///////////
URL : http://localhost:8090/api/automobileVOs/
- List all AutomobileVO  or individual
- Method: GET


______________________________________________________________________________________________________________________________________
///////////SalesPerson Insomnia///////////
URL : http://localhost:8090/api/sales_persons/

- List all SalesPerson or individual (Make sure to include the salesperson's at the end of the url for individual)
- Method: GET


- Create a new salesPerson
- Method: POST
- EX for Insomnia JSON body input:
{
	"name": "Ron",
	"employee_number": 3
}


- Delete a salesPerson
- Method: DELETE
- Make sure to include the salesperson's at the end of the url
    -EX: http://localhost:8090/api/sales_persons/2


_______________________________________________________________________________________________________________________________________
///////////Customer Insomnia///////////
URL : http://localhost:8090/api/customers/
 - List all customers or individual (Make sure to include customer's ID at the end of url)
 - Method: GET


 Create a new customer
- Method: POST
- EX for Insomnia JSON body input:
{
	"name": "Bunta",
	"address": "1111 Trueno blvd",
	"phone_number": "8583301111"
}


Delete a customer
- Method: DELETE
Make sure to include customer's ID at the end of url
    -EX: http://localhost:8090/api/customers/3

__________________________________________________________________________________________________________________________///////////SalesRecord Insomnia///////////
URL : http://localhost:8090/api/sales_records/
- List all sales records
- Method: GET


//NOTE FOR CREATING A NEW SALES RECORD//
    - Make sure that you are using the sales_person's ID Number
    - Make sure that you are using the customer's ID Number
    - Make sure that you are using the automobile's VIN number
Create a new sales record
- Method: POST
- EX for Insomnia JSON body input:
{
	"sales_person" : 1,
	"customer" : 1,
	"automobile" : "1C3CC5FB2AN120174",
	"sales_price": "9000"
}

__________________________________________________________________________________________________________________________



## Service microservice

## Design
-----Service Design-----

![](/Images/Service.png)
-----INSOMNIA FOR Service-----

---KEY NOTES---
-Order of creation for an automobile
1. Manufacturer
2. Vehicle Model
3. Automobile


-A service record CAN NOT be created until the following are in the database for selection
1. AutomobileVO with pertinent information from Poller
2. Technician
3. Appointment


______________________________________________________________________________________________________________________________________
///////////AutomobileVO Insomnia///////////
URL : http://localhost:8090/api/automobileVOs/
- List all AutomobileVO  or individual
- Method: GET


______________________________________________________________________________________________________________________________________
///////////Technician Insomnia///////////
URL : http://localhost:8080/api/techs/

- List all Technicians(Make sure to include the technician number at the end of the url for individual)
- Method: GET


- Create a new technician
- Method: POST
- EX for Insomnia JSON body input:
{
	"name": "ben",
	"employee_number": "2"
}

- Delete a Technician
- Method: DELETE
- Make sure to include the service person's employee number
    -EX: http://localhost:8090/api/techs/2


_______________________________________________________________________________________________________________________________________
///////////Appointment Insomnia///////////
URL : http://localhost:8080/api/listapps/
 - List all appointments (include appointment id)
 - Method: GET


 Create a new customer
- Method: POST
- EX for Insomnia JSON body input:
{
    "vin": "1C3CC5FB2AN120171",
    "id": 3,
    "customer_name": "mig",
    "date": "2032-10-27T04:43:14+00:00",
    "reason": "squeqaky",
    "is_finished": false,
    "is_vip": true,
    "auto_tech": 1
}


Delete a customer
- Method: DELETE
Make sure to include customer's ID at the end of url
    -EX: http://localhost:8090/api/appointment/3/

__________________________________________________________________________________________________________________________///////////ServiceHistory Insomnia///////////
URL : http://localhost:8080/api/service/
- List all sales records
- Method: GET


//NOTE FOR CREATING A NEW SALES RECORD//
    - Make sure that you are using the technician's ID Number
    - Make sure that you are using the owner's ID Number
    - Make sure that you are using the automobile's VIN number
Create a new sales record
- Method: POST
- EX for Insomnia JSON body input:
{
	"vin": "1C3CC5FB2AN120171",
	"id": 2,
	"customer_name": "me1g",
	"date": "2032-10-27T04:43:14+00:00",
	"reason": "squeqaky",
	"is_finished": false,
	"is_vip": true,
	"auto_tech": 1
}
    -EX: http://localhost:8090/api/appointment/1C3CC5FB2AN120171/

__________________________________________________________________________________________________________________________
