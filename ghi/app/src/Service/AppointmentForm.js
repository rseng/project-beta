import React, { useEffect, useState } from "react";

const AppointmentForm = () => {
    const [techs, setTechs] = useState([]);
    const [owner, setOwner] = useState("");
    const [selectedTechnician, setSelectedTechnician] = useState("");
    const [dateTime, setDateTime] = useState("");
    const [reason, setReason] = useState([]);
    const [submitted, setSubmitted] = useState(false);
    const [vin, setVin] = useState("");


    useEffect(() => {
        const getTechs = async () => {
            const url = "http://localhost:8080/api/techs/"
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setTechs(data.techs);
            }
        };
        getTechs();
    }, []);

    const clearAppointment = () => {
        setVin("");
        setOwner("");
        setDateTime("");
        setSelectedTechnician("");
        setReason("");
        setSubmitted(true);
    };

    //DOM intialization, loading data from listapps
    const handleSubmit = async (event) => {
        event.preventDefault();
        const TimeDate = dateTime;
        const autoTech = selectedTechnician;
        const data = { vin, owner, TimeDate, autoTech, reason };
        const url = "http://localhost:8080/api/listapps/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            event.target.reset();
            clearAppointment();
        }
    };
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create A New Appointment</h1>
                    <form id="create-appointment" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                onChange={(e) => setVin(e.target.value)} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                            <label htmlFor="vin">Vin Number</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={(e) => setOwner(e.target.value)} placeholder="Owner" required type="text" name="Owner" id="owner" className="form-control" />
                            <label htmlFor="owner">Owner Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={(e) => setDateTime(e.target.value)} placeholder="Date Time" required type="datetime-local" name="dateTime" id="dateTime" className="form-control" />
                            <label htmlFor="Date Time">Date and Time</label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={(e) => setSelectedTechnician(e.target.value)}
                                required
                                name="technician"
                                id="technician"
                                className="form-select">
                                <option value="">Select a Technician</option>
                                {techs.map((tech) => {
                                    return (
                                        <option key={tech.id} value={tech.id}>
                                            {tech.name}
                                        </option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="reason">Reason</label>
                            <textarea
                                onChange={(e) => setReason(e.target.value)} id="reason" rows="1" name="reason" className="form-control"></textarea>
                        </div>
                        <div className="col text-center">
                            <button className="btn btn-primary">Create</button>
                        </div>
                    </form>
                    {submitted && (
                        <div
                            className="alert alert-success mb-0 p-4 mt-4"
                            id="success-message">
                            Appointment Booked Succesful
                        </div>
                    )}
                </div>
            </div>
        </div>
    );

};


export default AppointmentForm;
