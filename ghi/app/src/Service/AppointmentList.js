import { NavLink } from 'react-router-dom';
import React, { useEffect, useState } from "react";

function ServiceList() {
    const [service, setService] = useState([]);

    useEffect(() => {
        const getService = async () => {
            const response = await fetch("http://localhost:8080/api/listapps/");
            console.log("done")
            const data = await response.json();
            console.log("DATA", data)
            console.log(data.appointment[0])
            console.log(data.appointment.length)
            setService(data)
            setService(data)
            setService(data => data.appointment.filter((app) => app.is_finished !== "true"))
            for (let i in data.appointment.length) {
                console.log("done")
            }
        }
        getService()
    }, [])

    const AppointmentClear = async (id) => {
        const serviceurl = `http://localhost:8080/api/listapps/${id}`;
        const fetchConfig = {
            method: "DELETE",
            headers: {
                "Content-Type": "application/json",
            },
        };
        const response = await fetch(serviceurl, fetchConfig);
        if (response.ok) {
            window.location.reload();
        }
    };
    const AppointmentCertify = async (id) => {
        const serviceurl = `http://localhost:8080/api/listapps/${id}`;
        const fetchConfig = {
            method: "PUT",
            body: JSON.stringify({ is_finished: true }),
            headers: {
                "Content-Type": "application/json"
            },
        };
        const response = await fetch(serviceurl, fetchConfig)
        if (response.ok) {
            window.location.reload();
        }
    };
    return (
        <div>
            <h1>Service Appointments</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>VIP</th>
                        <th>Customer Name</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Auto Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {service.map((appointment) => {
                        return (
                            <tr key={appointment.id}>
                                <td>{appointment.vin}</td>
                                <td>{String(appointment.is_vip)}</td>
                                <td>{appointment.customer_name}</td>
                                <td>{
                                    new Date(appointment.date).toLocaleDateString()
                                }</td>
                                <td>{
                                    new Date(appointment.date).toLocaleTimeString()
                                }
                                </td>
                                <td>{appointment.auto_tech.name}</td>
                                <td>{appointment.reason}</td>
                                <td>
                                    <button onClick={() => AppointmentClear(appointment.id)}> Cancel </button>
                                </td>
                                <td>
                                    <button onClick={() => AppointmentCertify(appointment.id)}> Finished </button>
                                </td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default ServiceList;
