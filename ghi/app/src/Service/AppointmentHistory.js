import React, { useState, useEffect } from "react"
function AppointmentHistory(app) {
    const [appointment, setApps] = useState([]);
    const [searchVin, setSearchVin] = useState("");
    const [filtered, setFiltered] = useState([]);
    const [submitted, setSubmitted] = useState(false);

    useEffect(() => {
        const getAppointmentHistory = async () => {
            const response = await fetch("http://localhost:8080/api/service/");
            if (response.ok) {
                const data = await response.json();
                setApps(data.appointment);
            }
        }
        getAppointmentHistory()
    }, []);
    const handleSearch = async (event) => {
        const results = appointment.filter((appointment) =>
            appointment.vin.includes(searchVin)
        );
        setFiltered(results);
        setSubmitted(true);
    };
    return (
        <React.Fragment>
            <div className="px-4 py-5 my-1 mt-0 text-center">
                <h1 className="display-5">VIN Service Appointments</h1>
            </div>
            <div className="row height d-flex justify-content-center align-items-center">
                <div className="col-md-auto">
                    <div className="input-group mb-2">
                        <input type="text" value={searchVin} onChange={(e) => setSearchVin(e.target.value)} />
                        <button onClick={handleSearch} type="button" className="btn btn-outline-secondary"> Search VIN </button>
                    </div>
                </div>
            </div>
            {filtered.length > 0 && (
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Customer Name</th>
                            <th>VIN</th>
                            <th>Date</th>
                            <th>Time</th>
                            <th>Auto Technician</th>
                            <th>Reason</th>
                            <th>Finished</th>
                        </tr>
                    </thead>
                    <tbody>
                        {filtered.map((filter) => {
                            return (
                                <tr className="table-row" key={filter.id}>
                                    <td>{filter.customer_name}</td>
                                    <td>{filter.vin}</td>
                                    <td>{new Date(filter.date).toLocaleDateString("en-US")}</td>
                                    <td>{new Date(filter.date).toLocaleTimeString([], { hour: "2-digit", minute: "2-digit", })}</td>
                                    <td>{filter.auto_tech.name}</td>
                                    <td>{filter.reason}</td>
                                    <td>{filter.is_finished ? true : false} </td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            )}
            {submitted && filtered.length === 0 && (
                <div className="alert alert-danger mb-0 p-4 mt-4" id="danger-message">
                    The VIN you entered has no appointment history.
                </div>
            )}
        </React.Fragment>
    );
};

export default AppointmentHistory;
