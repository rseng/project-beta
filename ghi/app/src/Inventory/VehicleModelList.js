import React from 'react'

const initialState = {
    models: []
  };

class VehicleModelList extends React.Component {
  constructor(props){
    super(props)
    this.state= initialState
  }
  async componentDidMount() {
    const url = "http://localhost:8100/api/models/"
    const response = await fetch(url)

    if (response.ok) {
      const data = await response.json()
      this.setState({ models: data.models })
    }
  }

  render() {
    return (
      <>
        <h1 align="center" className="dislplay-1 text-center" >Vehicle Models</h1>
        <table className="table align-middle">
          <thead>
            <tr>
              <th>Manufacturer</th>
              <th>Name</th>
              <th>Picture</th>
            </tr>
          </thead>
          <tbody>
          {this.state.models?.map(model => {
              return (
                <tr key={model.id}>
                  <td>{model.manufacturer.name}</td>
                  <td>{model.name}</td>
                  <td><img src={model.picture_url} width={200} height={150}/></td>
                </tr>
              )
            })}
          </tbody>
        </table>
      </>
    )
  }
}

export default VehicleModelList
