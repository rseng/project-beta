import React from "react";

function ManufacturerList(props) {
    console.log(props)
    return (
        <div className="Manufacturer-list">
            <h1 className ="title" align="center"> List of Manufacturers</h1>
            <table className ="table align-middle">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {props.manufacturers.map(manufacturer => {
                        return (
                            <tr key ={manufacturer.id}>
                                <td>{manufacturer.name}</td>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}

export default ManufacturerList
