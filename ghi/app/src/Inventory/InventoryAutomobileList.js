import React from "react";

function InventoryAutomobileList(props) {
    {console.log(props)}
    return (
        <div className="car-list">
            <h1 className="title" align="center">List of cars</h1>
            <table className="table align-middle">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Model</th>
                        <th>Vin Number</th>
                        <th>Year</th>
                        <th>Color</th>
                        <th> Photo </th>
                    </tr>
                </thead>
                <tbody>
                    {props.automobiles?.map(car => {
                        return (
                            <tr key={car.id}>
                                <td>{car.model.manufacturer.name}</td>
                                <td>{car.model.name}</td>
                                <td>{car.vin}</td>
                                <td>{car.year}</td>
                                <td>{car.color}</td>
                                <td><img src={car.model.picture_url} heigh={100}/></td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default InventoryAutomobileList
