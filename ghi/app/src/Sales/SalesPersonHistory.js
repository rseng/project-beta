import React from "react";

class SalesPersonHistory extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            sales_records:[],
            list:[]
        }
    }
    handleSalesPersonChange = (event) => {
        const value = event.target.value;
        this.setState({ sales_person: parseInt(value) })
    }

    async componentDidMount() {
        const sales_Person_url = 'http://localhost:8090/api/sales_persons/';
        const sales_Detail_url = 'http://localhost:8090/api/sales_records/';



        const responseSalesPerson = await fetch(sales_Person_url)
        const responseSalesDetail = await fetch(sales_Detail_url)



        if (responseSalesPerson.ok) {
            const SalesPersonData = await responseSalesPerson.json();

            this.setState({ list: SalesPersonData.list_of_sales_person })
        }

        if (responseSalesDetail.ok) {
            const data = await responseSalesDetail.json();

            this.setState({ sales_records: data.sales_list })
        }
    }


    render() {
        return (
            <>
                <h1 className="mt-5">Sales Person History</h1>
                <select onChange={this.handleSalesPersonChange} required id="a_sales_person" value={this.state.sales_person} name="a_sales_person" className="form-select mt-3">
                    <option value="">Select a Sales Person</option>
                    {this.state.list?.map(person => {
                        return (
                            <option key={person.id} value={person.employee_number}>
                                {person.name}
                            </option>
                        );
                    })}
                </select>

                <div>
                    <table className="table table-striped mt-3">
                        <thead>
                            <tr>
                                <th>Sales Person</th>
                                <th>Customer</th>
                                <th>VIN</th>
                                <th>Sale Price</th>
                            </tr>
                        </thead>
                        <tbody id="sales_person_details">
                            {this.state.sales_records.filter(sales_records => sales_records.sales_person.employee_number === this.state.sales_person).map(record => {
                                return(
                                    <tr key={record.automobile.import_vin}>
                                        <td>{record.sales_person.name}</td>
                                        <td>{record.customer.name}</td>
                                        <td>{record.automobile.import_vin}</td>
                                        <td>{record.sales_price}</td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            </>
        )
    }
}
export default SalesPersonHistory
