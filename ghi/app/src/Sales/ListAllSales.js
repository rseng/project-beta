import React from "react";


class ListAllSales extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            records:[]
        }
    }


    async componentDidMount() {
        const url = "http://localhost:8090/api/sales_records/";
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            this.setState({records: data.sales_list});
        };
    }

    render() {
        return (
            <div>
                <h2 className="mt-5"><b>All Sales Records</b></h2>
                <table className="table table-striped mt-3">
                    <thead>
                        <tr>
                            <th>Sales Person</th>
                            <th>Employee Number ID</th>
                            <th>Customer Name</th>
                            <th>Automobile VIN</th>
                            <th>Sales Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.records.map(record => {
                            {console.log("RECORD",record)}
                            return (
                                <tr key={record.id}>
                                    <td>{ record.sales_person.name }</td>
                                    <td>{ record.sales_person.employee_number }</td>
                                    <td>{ record.customer.name }</td>
                                    <td>{ record.automobile.import_vin }</td>
                                    <td>{ record.sales_price }</td>
                                </tr>
                            );
                        })}
                    </tbody>
                </table>
            </div>
        )
    }
}





export default ListAllSales




