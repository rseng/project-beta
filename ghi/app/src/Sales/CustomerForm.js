import React from "react";

const initialState = {
    name: '',
    address: '',
    phone_number: '',
};


class CustomerForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = initialState
    };


    handleCustomerChange = (event) => {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleAddressChange = (event) => {
        const value = event.target.value;
        this.setState({ address: value })
    }

    handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        this.setState({ phone_number: value})
    }

    handleSubmit = async(event)=>{
        event.preventDefault();

        const data = {...this.state}


        const postUrl = 'http://localhost:8090/api/customers/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': ' application/json',
            },
        };

        const response = await fetch(postUrl, fetchConfig);
        if (response.ok) {
            const new_customer = await response.json();

            this.setState(initialState);
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a customer</h1>
                        <form onSubmit={this.handleSubmit} id="create-a-customer-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleCustomerChange} placeholder="Name" value={this.state.name} required type="text" name="name" id="name" className="form-control"></input>
                                <label htmlFor="name">Customer</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleAddressChange} placeholder="Address" value={this.state.address} required type="text" name="address" id="address" className="form-control"></input>
                                <label htmlFor="address">Address</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handlePhoneNumberChange} placeholder="Phone_number" value={this.state.phone_number} required type="text" name="phone_number" id="phone_number" className="form-control"></input>
                                <label htmlFor="phone_number">Phone Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}





export default CustomerForm
