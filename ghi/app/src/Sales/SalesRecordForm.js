import React from "react";

const initialState = {
    automobiles: [],
    sales_persons: [],
    customers: [],
    customer:"",
    sales_person:"",
    automobile:""
};


class SalesRecordForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = initialState
    };

    handleAutomobileChange = (event) => {
        const value = event.target.value;
        this.setState({ automobile: value })
    }

    SalesPersonsChange = (event) => {
        const value = event.target.value;
        this.setState({ sales_person: value })
    }

    handleCustomerChange = (event) => {
        const value = event.target.value;
        this.setState({ customer: value })
    }

    handleSalesPriceChange = (event) => {
        const value = event.target.value;
        this.setState({ sales_price: value })
    }




    handleSubmit = async(event) => {
        event.preventDefault();

        const data = {...this.state}
        delete data.automobiles;
        delete data.sales_persons;
        delete data.customers;
        console.log("REALDATA",data)


        const postUrl = 'http://localhost:8090/api/sales_records/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': ' application/json',
            },
        };

        const response = await fetch(postUrl, fetchConfig);
        if (response.ok) {
            const newSalesRecord = await response.json();

            this.setState(initialState);
        }
    }


    async componentDidMount() {
        const sales_Person_url = 'http://localhost:8090/api/sales_persons/';
        const AutomobileVOurl = 'http://localhost:8090/api/automobileVOs/';
        const customerurl = 'http://localhost:8090/api/customers/';

        const responseSalesPerson = await fetch(sales_Person_url)
        const responseAutomobile = await fetch(AutomobileVOurl)
        const responseCustomer = await fetch(customerurl)

        if (responseSalesPerson.ok) {
            const data = await responseSalesPerson.json();

            this.setState({ sales_persons: data.list_of_sales_person })
        }

        if (responseAutomobile.ok) {
            const data = await responseAutomobile.json();
            console.log("DATA::::", data)

            this.setState({ automobiles: data.automobiles })
        }

        if (responseCustomer.ok) {
            const data = await responseCustomer.json();

            this.setState({ customers: data.customers })


        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Record a new sale</h1>
                        <form onSubmit={this.handleSubmit} id="create-a-sales-record-form">


                            <div className="mb-3">
                                <select onChange={this.handleAutomobileChange} required name="car" value={this.state.automobile} id="car" className="form-select">
                                    <option value="">Choose an Automobile</option>
                                    {this.state.automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.id} value={automobile.import_vin}>
                                                {automobile.import_vin}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>

                            <div className="mb-3">
                                <select onChange={this.SalesPersonsChange} required name="person" value={this.state.sales_person} id="person" className="form-select">
                                    <option value="">Choose a Sales Person</option>
                                    {this.state.sales_persons.map(sales_person => {
                                        return (
                                            <option key={sales_person.id} value={sales_person.id}>
                                                {sales_person.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>

                            <div className="mb-3">
                                <select onChange={this.handleCustomerChange} required name="customer" value={this.state.customer} id="customer" className="form-select">
                                    <option value="">Choose a Customer</option>
                                    {this.state.customers.map(customer => {
                                        return (
                                            <option key={customer.id} value={customer.id}>
                                                {customer.name}
                                            </option>
                                        )
                                    })}
                                </select>
                            </div>

                            <div className="form-floating mb-3">
                                <input onChange={this.handleSalesPriceChange} placeholder="Sales Price" value={this.state.sales_price}  required type="text" name="sales_price" id="sales_price" className="form-control" />
                                <label htmlFor="sales_price">Sales Price</label>
                             </div>



                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

export default SalesRecordForm
