import AppointmentsList from './AppointmentList';
import CustomerForm from './Sales/CustomerForm';
import SalesPersonForm from './Sales/SalesPersonForm';
import ListAllSales from './Sales/ListAllSales'
import SalesPersonHistory from './Sales/SalesPersonHistory';
import ManufacturerList from './Inventory/ManufacturerList';
import InventoryAutomobileList from './Inventory/InventoryAutomobileList';
import VehicleModelList from './Inventory/VehicleModelList';
import SalesRecordForm from './Sales/SalesRecordForm'
import ServiceList from './Service/AppointmentList';
import AppointmentForm from './Service/AppointmentForm';
import TechnicianForm from './Service/TechnicianForm';
import AppointmentHistory from './Service/AppointmentHistory';

function App(props) {
  if (props.manufacturers === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/listapps/" element={<AppointmentsList />} />

          <Route path="manufacturers">
            <Route path="" element={<ManufacturerList manufacturers={props.manufacturers} />} />
          </Route>

          <Route path="VehicleModels">
            <Route path="" element={<VehicleModelList />} />
          </Route>

          <Route path="inventory">
            <Route path="" element={<InventoryAutomobileList automobiles={props.automobiles} />} />
          </Route>

          <Route path="sales">
            <Route path="" element={<SalesRecordForm />} />
            <Route path="new_customer" element={<CustomerForm />} />
            <Route path="new_sales_person" element={<SalesPersonForm />} />
            <Route path="list_all_sales" element={<ListAllSales />} />
            <Route path="sales_persons_history" element={<SalesPersonHistory />} />
            <Route path="/listapps/" element={<ServiceList />} />
            <Route path="/postapps/" element={<AppointmentForm />} />
            <Route path="/tech/" element={<TechnicianForm />} />
            <Route path="/apphistory/" element={<AppointmentHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
