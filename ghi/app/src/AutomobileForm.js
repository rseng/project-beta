import { NavLink } from 'react-router-dom';
import React from "react";

import React from "react";

const initialState = {
    name: '',
    employee_number: '',
};


class SalesPersonForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = initialState


    }


    handleNamehange = (event) => {
        const value = event.target.value;
        this.setState({ name: value })
    }

    handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        this.setState({ employee_number: value })
    }

    handleSubmit = async (event) => {
        event.preventDefault();

        const data = { ...this.state }


        const postUrl = 'http://localhost:8090/api/sales_persons/'
        const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': ' application/json',
            },
        };

        const response = await fetch(postUrl, fetchConfig);
        if (response.ok) {
            const new_customer = await response.json();

            this.setState(initialState);
        }
    }


    render() {
        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                        <h1>Create a customer</h1>
                        <form onSubmit={this.handleSubmit} id="create-a-sales-person-form">
                            <div className="form-floating mb-3">
                                <input onChange={this.handleNamehange} placeholder="Name" value={this.state.name} required type="text" name="name" id="name" className="form-control"></input>
                                <label htmlFor="name">Sales Person</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input onChange={this.handleEmployeeNumberChange} placeholder="Employee_number" value={this.state.employee_number} required type="text" name="employee_number" id="employee_number" className="form-control"></input>
                                <label htmlFor="employee_number">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}





export default SalesPersonForm
