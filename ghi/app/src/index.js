import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);




async function loadAllApi() {
  const response = await fetch('http://localhost:8100/api/automobiles/')
  const ModelResponse = await fetch('http://localhost:8100/api/models/')
  const MFGResponse = await fetch('http://localhost:8100/api/manufacturers/')

  if (response.ok) {
    const automobileData = await response.json()
    const modelData = await ModelResponse.json()
    const MFGData = await MFGResponse.json()
    root.render(
      <React.StrictMode>
        <App manufacturers = {MFGData.manufacturers}
        models = {modelData.models}
        automobiles = {automobileData.autos} />
      </React.StrictMode>
    )
  } else{
    console.log("Error", response)
  }
}
loadAllApi()



