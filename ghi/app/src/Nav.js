import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light">
      <div className="container-fluid">
        <NavLink className="navbar-brand main-title" to="/"><b>CARCAR</b></NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>

        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <li className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item dropdown">
              <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a1">
                <b>Inventory</b>
              </p>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturers">Manufacturers List</NavLink></li>
                <div className="dropdown-divider"></div>
                <li><NavLink className="dropdown-item" to="/VehicleModels">Vehicle Models List</NavLink></li>
                <div className="dropdown-divider"></div>
                <li><NavLink className="dropdown-item" to="/inventory">Automobiles List</NavLink></li>
              </ul>
            </li>
            <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a3">
              <b>Technician and Service</b>
            </p>
            <ul className="dropdown-menu">
              <li><NavLink className="dropdown-item" to="/service/listapps/">View all Service Appointments</NavLink></li>
              <div className="dropdown-divider"></div>
              <li><NavLink className="dropdown-item" to="/service/postapps/">Add an Appointment</NavLink></li>
              <div className="dropdown-divider"></div>
              <li><NavLink className="dropdown-item" to="/service/tech/">Make a new tech</NavLink></li>
              <div className="dropdown-divider"></div>
              <li><NavLink className="dropdown-item" to="/service/apphistory/">View vehicle History</NavLink></li>
            </ul>
          </li>
          <li className="nav-item dropdown">
            <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a3">
              <b>Sales Records</b>
            </p>
            <ul className="dropdown-menu">
              <li><NavLink className="dropdown-item" to="/sales/list_all_sales">View all Sales History</NavLink></li>
              <div className="dropdown-divider"></div>
              <li><NavLink className="dropdown-item" to="/sales/sales_persons_history">Sales History</NavLink></li>
              <div className="dropdown-divider"></div>
              <li><NavLink className="dropdown-item" to="/sales/">Make a new sale</NavLink></li>
              <div className="dropdown-divider"></div>
              <li><NavLink className="dropdown-item" to="/sales/new_customer">Add a customer</NavLink></li>
            </ul>
          </li>
          <li className="nav-item dropdown">
            <p className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false" id="a2">
              <b>Salesman</b>
            </p>
            <ul className="dropdown-menu">
              <li><NavLink className="dropdown-item" to="/sales/new_sales_person">Add a sales rep</NavLink></li>
            </ul>
          </li>
        </div>
      </div>
    </nav >
  )
}

export default Nav;
